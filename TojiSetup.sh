git clone https://ccerrato147@bitbucket.org/ccerrato147/toji.docker-compose.git Toji
git clone https://ccerrato147@bitbucket.org/ccerrato147/toji.frontend.git Toji/FrontEnd
git clone https://ccerrato147@bitbucket.org/ccerrato147/toji.web.git Toji/TojiWebApp
git clone https://ccerrato147@bitbucket.org/ccerrato147/toji.ubuntureactexpress.git Toji/UbuntuReactExpress
git clone https://ccerrato147@bitbucket.org/ccerrato147/toji.mongodockerimage.git Toji/MongoDockerImage
docker build -t ccerrato147/toji Toji/UbuntuReactExpress/
docker build -t ccerrato147/mongo Toji/MongoDockerImage/
cd Toji/TojiWebApp
npm install
